(type Directory (init (path inode (event inode.event))) 
  (gett path (inode) inode.path)
  (gett keys (path) (.resolve Future.Array (readdir path)))

  (remember-map-from keys children (key) (.get this key)))

(let directory (create Directory))
(generics (evented def-method Directory)

            ( make  ( inode (path inode.path))
                    "given an inode, extract a path from the inode and create a directory at this location. Will return as failed
if the directory already exists, or the operation fails for some other reason. Call this instead of fill if you expect there to not be a directory, and you want to fail if there is."
                    (then-do (mkdir path ) inode))

            ( fill  (inode (path inode.path) (filled "/"))
                    "given an inode, extract a path from the inode and create a directory at this location. Will not fail
if the call to `mkdir` fails. Call this instead of make if you expect there may already be a directory, and you just want to make sure."
                    (def safely-mkdir (path-element)
                      (assign filled (Path.join filled path-element))
                      (pipe (mkdir filled)
                            (then-do (inode.system.add filled))
                            (catch false)))
                    (.map-serial Future.Array
                                 safely-mkdir
                                 (.split (Path.resolve path)
                                         Path.sep)))

            ( delete  (inode (path inode.path))
                      "Simply destroy the directory indexed by the inode given."
                      (rmdir path)))

(comment (let d ((create Directory)))
         ;;creates an instance of d
         (then (.make d "path/to/thing"))) 
(export Directory )
